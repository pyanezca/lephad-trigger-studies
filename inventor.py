import ROOT as R
import os
from argparse import ArgumentParser

#local includes
import assistant

parser = ArgumentParser('', add_help=False)
parser.add_argument( '-s', '--sample', help = 'Choose your sample (ggf1, ggf10 or vbf1 currently)', default = None)
result = parser.parse_args()

assert result.sample in ["ggF1lh","ggf1", "ggf10", "vbf1"], "Sample not available"

# read input file
INPATH = os.path.join(os.getenv("INV_NTUP_PATH"), result.sample + ".root") # path needs to be set before running
print "Reading file:", INPATH
assert os.path.isfile(INPATH), "Which isn't actually a file. Please come again."
INFILE = R.TFile(INPATH, "READ")

# dig out the tree
treeName = "ntuple"
tree = INFILE.Get(treeName)
assert isinstance(tree, R.TTree), "No tree named " + treeName + " in " + INPATH
nEntries = tree.GetEntriesFast()
print "Tree in input file contains", nEntries, "events"

jetCollDict = {}
jetCollDict["Run2"] = "Run2_L1Jet"
jetCollDict["Run3"] = "jRoundJetsPUsub"

# prepare histograms to be filled, and the output file
outfile = R.TFile(result.sample + "_hists.root", "RECREATE")

# define combined trigger objects to investigate. First part is tau energy cuts, then jet energy cuts, then topological cuts
combTrigDict = {}
# when implementing existing triggers, check whether it's a hyphen or underscore in the trigger item name, meaning whether the tau and jet part are to be fulfilled independently (= underscore, no OLR of jets with taus) or simultaneously (= hypen, then we need OLR)
combTrigDict["denom"] = assistant.CombinedTrigger("denom", [], [], [], [], False, []) # empty trigger, for eff denominator. Does nothing
combTrigDict["L1Topo"] = assistant.CombinedTrigger("L1Topo", [], ["L1_TAU20", "L1_TAU12"], ["L1_J25"], ["T_DR_28"], True, ["J_PT_80", "T_DR_25"]) # dr(tau, tau) < 2.8
#combTrigDict["4J12"] = assistant.CombinedTrigger("4J12", [], ["L1_TAU20", "L1_TAU12"], ["L1_J12", "L1_J12", "L1_J12", "L1_J12"], ["J_ETA_23"], False, ["J_PT_45:45"]) # jet eta cut of 2.5 (or 2.3 in 2018)
#combTrigDict["5J12"] = assistant.CombinedTrigger("5J12", [], ["L1_TAU12", "L1_TAU12"], ["L1_J12", "L1_J12", "L1_J12", "L1_J12", "L1_J12"], [], False, []) # for testing, would still need offl cuts
#combTrigDict["6J12"] = assistant.CombinedTrigger("6J12", [], ["L1_TAU12", "L1_TAU12"], ["L1_J12", "L1_J12", "L1_J12", "L1_J12", "L1_J12", "L1_J12"], [], False, []) # for testing, would still need offl cuts
combTrigDict["3J12LepHad"] = assistant.CombinedTrigger("3J12LepHad", ["L1_EM15"], ["L1_TAU12","L1_TAU12"], ["L1_J12","L1_J12","L1_J12"], [], True, [])

# histograms to be produced (split into Run2 and Run3, and split by which trigger is applied)
integralDict = {}
histDict = {}
for run in ["Run2", "Run3"]:
    integralDict[run] = {}
    histDict[run] = {}
    for trigName in combTrigDict.keys():
        integralDict[run][trigName] = 0.
        histDict[run][trigName] = {}
        histDict[run][trigName]["nJ12"] = assistant.initHist(run + "_" + trigName + "_" + "nJ12", "Number of jets passing L1_J12 (after tau OLR)", 11, -0.5, 10.5)
        histDict[run][trigName]["nJ25"] = assistant.initHist(run + "_" + trigName + "_" + "nJ25", "Number of jets passing L1_J25 (after tau OLR)", 11, -0.5, 10.5)

        histDict[run][trigName]["leadDRtt"] = assistant.initHist(run + "_" + trigName + "_" + "leadDRtt", "#Delta R_{#tau#tau} (lead candidates)", 20, 0, 4.5)
        histDict[run][trigName]["leadDEtatt"] = assistant.initHist(run + "_" + trigName + "_" + "leadDEtatt", "#Delta #eta_{#tau#tau} (lead candidates)", 20, 0, 4)
        histDict[run][trigName]["leadDPhitt"] = assistant.initHist(run + "_" + trigName + "_" + "leadDPhitt", "#Delta #phi_{#tau#tau} (lead candidates)", 20, 0, 3.2)
        histDict[run][trigName]["leadInvMasstt"] = assistant.initHist(run + "_" + trigName + "_" + "leadInvMasstt", "Visible M_{#tau#tau} (lead candidates)", 20, 0, 250000)

        histDict[run][trigName]["passDRtt"] = assistant.initHist(run + "_" + trigName + "_" + "passDRtt", "#Delta R_{#tau#tau} (lead passing candidates)", 20, 0, 4.5) # not for effs
        histDict[run][trigName]["passDEtatt"] = assistant.initHist(run + "_" + trigName + "_" + "passDEtatt", "#Delta #eta_{#tau#tau} (lead passing candidates)", 20, 0, 4) # not for effs
        histDict[run][trigName]["passDPhitt"] = assistant.initHist(run + "_" + trigName + "_" + "passDPhitt", "#Delta #phi_{#tau#tau} (lead passing candidates)", 20, 0, 3.2) # not for effs
        histDict[run][trigName]["passInvMasstt"] = assistant.initHist(run + "_" + trigName + "_" + "passInvMasstt", "Visible M_{#tau#tau} (lead passing candidates)", 20, 0, 250000) # not for effs

        histDict[run][trigName]["minDRjj"] = assistant.initHist(run + "_" + trigName + "_" + "minDRjj", "Minimum #Delta R_{jj}", 20, 0, 4.5) # not for effs
        histDict[run][trigName]["minDEtajj"] = assistant.initHist(run + "_" + trigName + "_" + "minDEtajj", "Minimum #Delta #eta_{jj}", 20, 0, 4) # not for effs
        histDict[run][trigName]["minDPhijj"] = assistant.initHist(run + "_" + trigName + "_" + "minDPhijj", "Minimum #Delta #phi_{jj}", 20, 0, 3.2) # not for effs
        histDict[run][trigName]["maxInvMassjj"] = assistant.initHist(run + "_" + trigName + "_" + "maxInvMassjj", "Maximum M_{jj}", 20, 0, 500000) # not for effs

        histDict[run][trigName]["mHH"] = assistant.initHist(run + "_" + trigName + "_" + "mHH", "Visible M_{#tau#tau jj} (lead passing candidates)", 20, 0, 500000) # this should work, right? If we only OLR with the two taus here. Same for the lead single-object plots.

        for obj in ["ele0", "tau0", "tau1", "jet0", "jet1", "passele0", "passtau0", "passtau1", "passjet0", "passjet1"]: # single object kinematics
            histDict[run][trigName][obj] = assistant.initObjHistDict(run + "_" + trigName + "_" + obj)

histOfflJetPt = assistant.initHist("offlLeadJetPt", "Offline lead jet p_{T}", 20, 0, 200000)
histOfflTauDR = assistant.initHist("offlLeadTauDR", "Offline #Delta R_{#tau#tau}", 20, 0, 4.5)

# for scaling to proper number of expected events:
lumi = 139000. # take run2 lumi for now
xsec = assistant.getXsec(result.sample)
weightFileName = "sumOfWeights.txt"
sumOfWeights = assistant.readSumOfWeights(result.sample, weightFileName)
if sumOfWeights < 1e-7: # means the sum of weights wasn't found, so it needs to be calculated now
    print "Calculating sum of weights..."
    for ent in range(nEntries):
        tree.GetEntry(ent)
        sumOfWeights += tree.eventWeight
    print "Done, saving result and starting main calculation now"
    # save value here, so we don't need to recalculate
    with open(weightFileName, "a") as weightFile:
        weightFile.write(result.sample + "\t" + str(sumOfWeights) + "\n")

# start loop over events
noffl = 0
for ent in range(nEntries):
    tree.GetEntry(ent)
    print "NEntries: ", ent, " processed so far"
    # offline event selection, to define denominator efficiency measurements
    offlTaus = assistant.buildOfflTaus(tree.OffTaus_pt, tree.OffTaus_eta, tree.OffTaus_phi, tree.OffTaus_ntracks, tree.OffTaus_score)
    offlTaus = filter(lambda tau: tau.pt > 30000. and tau.ntracks in [1, 3] and tau.ID("loose"), offlTaus) # tau requirements
    offlEles = assistant.buildOfflEles(tree.OffEles_pt, tree.OffEles_eta, tree.OffEles_phi, tree.OffEles_ptcone, tree.OffEles_etcone, tree.OffEles_flag, tree.OffEles_tightflag, tree.OffEles_isoflag)
    offlEles = filter(lambda ele: ele.pt > 18000. , offlEles)
    truthTaus = assistant.buildTruthTaus(tree.TruthTaus_ptvis, tree.TruthTaus_etavis, tree.TruthTaus_phivis)
    truthEles = assistant.buildTruthEles(tree.TruthEles_pt, tree.TruthEles_eta, tree.TruthEles_phi)
    #print "Number of 1p/3p loose taus with 20 GeV pt:", len(offlTaus)
    if len(offlTaus) != 1: continue
    if len(offlEles) != 1: continue
    if len(truthEles) == 0: continue
    #if len(offlTaus) < 2: continue
    #if not (offlTaus[0].truthMatched(truthTaus) and offlTaus[1].truthMatched(truthTaus)): continue
    if not (offlTaus[0].truthMatched(truthTaus) and offlEles[0].truthMatched(truthEles)): continue
    # offline jet selection, want two central ones to be able to do b-tagging
    offlJets = assistant.buildOfflJets(tree.AntiKt4EMPFlowJets_pt, tree.AntiKt4EMPFlowJets_eta, tree.AntiKt4EMPFlowJets_phi)
    #print "Before OLR we have", len(offlJets), "jets"
    offlJets = filter(lambda jet: jet.OLR(offlTaus), offlJets)
    #print "After OLR we have", len(offlJets), "jets"
    if len(offlJets) < 2: continue
    goodJets = 0
    for offlJet in offlJets:
        if offlJet.pt > 20000. and abs(offlJet.eta) < 2.5: goodJets += 1
    #print "Before jet pt and eta cuts"
    if goodJets < 2: continue # need two central jets for b-tagging
    histOfflJetPt.Fill(offlJets[0].pt, tree.eventWeight)
    histOfflTauDR.Fill(offlTaus[0].p4.DeltaR(offlEles[0].p4), tree.eventWeight)
    # offline selection done, now investigating triggers
    #print "One event survived offl selection"
    noffl += 1
    for run in ["Run2", "Run3"]:
        weight = xsec*lumi*(tree.eventWeight/sumOfWeights)
        # build objects to cut on, depending on the run
        L1Taus = assistant.buildL1Taus(eval("tree." + run + "_L1Tau_Et"), eval("tree." + run + "_L1Tau_eta"), eval("tree." + run + "_L1Tau_phi"), eval("tree." + run + "_L1Tau_iso"))
        # build objects to cut on, depending on the run
        L1Eles = assistant.buildL1Eles(eval("tree." + run + "_L1Ele_Et"), eval("tree." + run + "_L1Ele_eta"), eval("tree." + run + "_L1Ele_phi"), eval("tree.Run3_L1Ele_passiso" if run == "Run3" else "tree.Run2_L1Ele_iso"), run)
        # require TAU20_TAU12 on the event
        L1Jets = assistant.buildL1Jets(eval("tree.jRoundJetsPUsub_pt" if run == "Run3" else "tree.Run2_L1Jet_Et"), eval("tree." + jetCollDict[run] + "_eta"), eval("tree." + jetCollDict[run] + "_phi"))
        # apply trigger
        for trigName, combTrig in combTrigDict.iteritems():
            #print trigName, " ", combTrig, " ", len(L1Eles), "", len(L1Taus), " ", len(L1Jets)
            combTrigDecision, elesPassingTrig, tausPassingTrig, jetsPassingTrig = combTrig.getCombDecision(L1Eles, L1Taus, L1Jets, run)
            if not combTrigDecision: continue # trigger cut
            if not combTrig.applyOfflCut(offlTaus, offlJets): continue # offline plateau cuts needed because of the trigger
            integralDict[run][trigName] += weight
            cleanedL1Jets = filter(lambda jet: jet.OLR(L1Taus[:2] if len(L1Taus) > 1 else L1Taus, run), L1Jets)
            # fill standard kinematics for (sub-)leading jets and taus, and (sub-)leading passing jets and taus
            for index, tau in enumerate(L1Taus):
                if index == 1: break
                tau.fillHistDict(histDict[run][trigName]["tau" + str(index)], weight)
                #cambios
            for index, ele in enumerate(L1Eles):
                if index == 1: break
                ele.fillHistDict(histDict[run][trigName]["ele" + str(index)], weight)
            for index, jet in enumerate(cleanedL1Jets):
                if index == 2: break
                jet.fillHistDict(histDict[run][trigName]["jet" + str(index)], weight)
            for index, tau in enumerate(tausPassingTrig):
                if index == 1: break
                tau.fillHistDict(histDict[run][trigName]["passtau" + str(index)], weight)
                #cambios
            for index, ele in enumerate(elesPassingTrig):
                if index == 1: break
                ele.fillHistDict(histDict[run][trigName]["passele" + str(index)], weight)
            for index, jet in enumerate(jetsPassingTrig):
                if index == 2: break
                jet.fillHistDict(histDict[run][trigName]["passjet" + str(index)], weight)
            # fill additional hists
            histDict[run][trigName]["nJ12"].Fill(len(filter(lambda jet: jet.fulfils("L1_J12"), L1Jets)), weight)
            histDict[run][trigName]["nJ25"].Fill(len(filter(lambda jet: jet.fulfils("L1_J25"), L1Jets)), weight)
            #cambios
            if len(L1Taus) >= 1 and len(L1Eles) >= 1:
                histDict[run][trigName]["leadDRtt"].Fill(L1Taus[0].p4.DeltaR(L1Eles[0].p4), weight)
                histDict[run][trigName]["leadDEtatt"].Fill(abs(L1Taus[0].eta - L1Eles[0].eta), weight)
                histDict[run][trigName]["leadDPhitt"].Fill(L1Taus[0].p4.DeltaPhi(L1Eles[0].p4), weight)
                histDict[run][trigName]["leadInvMasstt"].Fill((L1Taus[0].p4 + L1Eles[0].p4).M(), weight)
            if len(tausPassingTrig) >= 1 and len(elesPassingTrig) >= 1:
                histDict[run][trigName]["passDRtt"].Fill(tausPassingTrig[0].p4.DeltaR(elesPassingTrig[0].p4), weight)
                histDict[run][trigName]["passDEtatt"].Fill(abs(tausPassingTrig[0].eta - elesPassingTrig[0].eta), weight)
                histDict[run][trigName]["passDPhitt"].Fill(tausPassingTrig[0].p4.DeltaPhi(elesPassingTrig[0].p4), weight)
                histDict[run][trigName]["passInvMasstt"].Fill((tausPassingTrig[0].p4 + elesPassingTrig[0].p4).M(), weight)
            jetsForAnglePlots = filter(lambda jet: jet.fulfils("L1_J12"), cleanedL1Jets)
            if len(jetsForAnglePlots) > 1:
                pairList = assistant.makeListOfPairings(jetsForAnglePlots)
                histDict[run][trigName]["minDRjj"].Fill(min([iobj.p4.DeltaR(jobj.p4) for [iobj, jobj] in pairList]), weight)
                histDict[run][trigName]["minDEtajj"].Fill(min([abs(iobj.eta - jobj.eta) for [iobj, jobj] in pairList]), weight)
                histDict[run][trigName]["minDPhijj"].Fill(min([iobj.p4.DeltaPhi(jobj.p4) for [iobj, jobj] in pairList]), weight)
                histDict[run][trigName]["maxInvMassjj"].Fill(max([(iobj.p4 + jobj.p4).M() for [iobj, jobj] in pairList]), weight)
            fullVec = R.TLorentzVector(0, 0, 0, 0)
            #cambios
            for obj in (tausPassingTrig + jetsPassingTrig + elesPassingTrig):
                fullVec = fullVec + obj.p4
            histDict[run][trigName]["mHH"].Fill(fullVec.M(), weight)

print "Effective stat:", noffl, "events pass offline selection"
assistant.calculateEffs(integralDict)

outfile.Write()
outfile.Close()
