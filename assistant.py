import ROOT as R
import os
from subprocess import call

"""
    Helper functions for easier histogram handling
    and tree access defined here.

    Internal functions that are not supposed to be
    used outside of this script start with a "_".
"""

######## custom classes

class L1Tau(object):
    def __init__(self, et, eta, phi, iso):
        self.et = et
        self.eta = eta
        self.phi = phi
        self.iso = iso

        self.p4 = R.TLorentzVector()
        self.p4.SetPtEtaPhiM(self.et, self.eta, self.phi, 0.) # setting mass to 0, is likely not correct but correct mass is not available (I think) and should not be needed anywhere

    def fulfils(self, triggerName):
        if triggerName == "notrig":
            return True
        elif triggerName.startswith("L1_TAU"):
            if triggerName.endswith("IM"):
                imRequired = True
                threshold = 1000.*float(triggerName[6:-2])
            else:
                imRequired = False
                threshold = 1000.*float(triggerName[6:])

            isoPassed = self._imIso() if imRequired else True

            return self._etCut(threshold) and isoPassed
        else:
            print "Don't know how to process trigger name " + triggerName
            raise NotImplementedError

    def _etCut(self, threshold):
        return (self.et > threshold)

    def _imIso(self):
        return imIso(self.et, self.iso)

    def OLR(self, L1Eles, run):
        thresh = 0.3 if run == "Run3" else 0.2 # smaller tau ROIs in Run 3
        return min([self.p4.DeltaR(L1Ele.p4) for L1Ele in L1Eles]) > thresh

    def fillHistDict(self, histDict, weight):
        histDict["et"].Fill(self.et, weight)
        histDict["eta"].Fill(self.eta, weight)
        histDict["phi"].Fill(self.phi, weight)
        histDict["iso"].Fill(self.iso, weight)

class OfflTau(object):
    def __init__(self, pt, eta, phi, ntracks, score):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.ntracks = int(ntracks + 0.1)
        self.score = score

        self.p4 = R.TLorentzVector()
        self.p4.SetPtEtaPhiM(self.pt, self.eta, self.phi, 0.) # setting mass to 0, is likely not correct but correct mass is not available (I think) and should not be needed anywhere

    def ID(self, wp):
        return tauId(wp, self.ntracks, self.score)

    def truthMatched(self, truthTaus):
        return min([self.p4.DeltaR(truthTau.p4) for truthTau in truthTaus]) < 0.3

class L1Ele(object):
    def __init__(self, et, eta, phi, iso):
        self.et = et
        self.eta = eta
        self.phi = phi
        self.iso = iso

        self.p4 = R.TLorentzVector()
        self.p4.SetPtEtaPhiM(self.et, self.eta, self.phi, 0.) # setting mass to 0, is likely not correct but correct mass is not available (I think) and should not be needed anywhere

    def fulfils(self, triggerName):
        if triggerName == "notrig":
            return True
        elif triggerName.startswith("L1_EM"):
            if triggerName.endswith("IM"):
                imRequired = True
                threshold = 1000.*float(triggerName[6:-2])
            else:
                imRequired = False
                threshold = 1000.*float(triggerName[5:])

            isoPassed = self._imIso() if imRequired else True

            return self._etCut(threshold) and isoPassed
        else:
            print "Don't know how to process trigger name " + triggerName
            raise NotImplementedError

    def _etCut(self, threshold):
        return (self.et > threshold)

    def _imIso(self):
        return imIso(self.et, self.iso)

    def fillHistDict(self, histDict, weight):
        histDict["et"].Fill(self.et, weight)
        histDict["eta"].Fill(self.eta, weight)
        histDict["phi"].Fill(self.phi, weight)
        histDict["iso"].Fill(self.iso, weight)

class OfflEle(object):
    def __init__(self, pt, eta, phi, ptcone, etcone, flag, tightflag, isoflag):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.ptcone = ptcone
        self.etcone = etcone
        self.flag   = flag
        self.tightflag = tightflag
        self.isoflag   = isoflag

        self.p4 = R.TLorentzVector()
        self.p4.SetPtEtaPhiM(self.pt, self.eta, self.phi, 0.) # setting mass to 0, is likely not correct but correct mass is not available (I think) and should not be needed anywhere

    #def ID(self, wp):
    #    return tauId(wp, self.ntracks, self.score)

    def truthMatched(self, truthEles):
        return min([self.p4.DeltaR(truthEle.p4) for truthEle in truthEles]) < 0.3

class TruthTau(object):
    def __init__(self, pt, eta, phi):
        self.pt = pt
        self.eta = eta
        self.phi = phi

        self.p4 = R.TLorentzVector()
        self.p4.SetPtEtaPhiM(self.pt, self.eta, self.phi, 0.) # setting mass to 0, is likely not correct but correct mass is not available (I think) and should not be needed anywhere

class TruthEle(object):
    def __init__(self, pt, eta, phi):
        self.pt = pt
        self.eta = eta
        self.phi = phi

        self.p4 = R.TLorentzVector()
        self.p4.SetPtEtaPhiM(self.pt, self.eta, self.phi, 0.) # setting mass to 0, is likely not correct but correct mass is not available (I think) and should not be needed anywhere

class L1Jet(object):
    def __init__(self, et, eta, phi):
        self.et = et
        self.eta = eta
        self.phi = phi

        self.p4 = R.TLorentzVector()
        self.p4.SetPtEtaPhiM(self.et, self.eta, self.phi, 0.) # setting mass to 0, is likely not correct but correct mass is not available (I think) and should not be needed anywhere

    def OLR(self, L1Taus, run):
        thresh = 0.3 if run == "Run3" else 0.4 # smaller tau ROIs in Run 3
        return min([self.p4.DeltaR(L1Tau.p4) for L1Tau in L1Taus]) > thresh

    def fulfils(self, triggerName):
        if triggerName == "notrig":
            return True
        elif triggerName.startswith("L1_J"):
            threshold = 1000.*float(triggerName[4:])

            return self._etCut(threshold)
        else:
            print "Don't know how to process trigger name " + triggerName
            raise NotImplementedError

    def _etCut(self, threshold):
        return (self.et > threshold)

    def fillHistDict(self, histDict, weight):
        histDict["et"].Fill(self.et, weight)
        histDict["eta"].Fill(self.eta, weight)
        histDict["phi"].Fill(self.phi, weight)

class OfflJet(object):
    def __init__(self, pt, eta, phi):
        self.pt = pt
        self.eta = eta
        self.phi = phi

        self.p4 = R.TLorentzVector()
        self.p4.SetPtEtaPhiM(self.pt, self.eta, self.phi, 0.) # setting mass to 0, is likely not correct but correct mass is not available (I think) and should not be needed anywhere

    def OLR(self, Taus):
        thresh = 0.3
        return min([self.p4.DeltaR(Tau.p4) for Tau in Taus]) > thresh
"""
"""
class CombinedTrigger(object):
    def __init__(self, name, eleThresholds, tauThresholds, jetThresholds, topoCuts, olr, offlCuts):
        self.name = name
        self.eleThresholds = eleThresholds
        self.tauThresholds = tauThresholds
        self.jetThresholds = jetThresholds
        self.topoCuts = topoCuts
        self.olr = olr
        self.offlCuts = offlCuts

    def getCombDecision(self, eles, taus, jets, run):
        # no-trigger case first

        if (len(self.tauThresholds) == 0 and len(self.eleThresholds) == 0 and len(self.jetThresholds) == 0):
            return True, taus[:2] if len(taus) > 1 else taus, eles[:2] if len(eles) > 1 else eles, jets[:2] if len(jets) > 2 else jets

        if len(self.tauThresholds) == 1:
            tauCandidates = filter(lambda tau: tau.et > self.getMinThreshold(self.tauThresholds), taus) # apply min energy threshold to remove low-energy candidates and reduce combinations to check
            tauCandidates = [[tau] for tau in tauCandidates] # one-element lists to have same structure as when we have pairs
        elif len(self.tauThresholds) == 2:
            tauCandidates = makeListOfPairings(taus, minEnergy = self.getMinThreshold(self.tauThresholds))
        else:
            print "Three-tau triggers not yet implemented!"
            raise NotImplementedError

        #print "ele thresholds:", self.eleThresholds
        if len(self.eleThresholds) ==1:
            eleCandidates = filter(lambda ele: ele.et > self.getMinThreshold(self.eleThresholds), eles)
            eleCandidates = [[ele] for ele in eleCandidates]
        elif len(self.eleThresholds) ==0:
            eleCandidates = []

        jetCandidates = filter(lambda jet: jet.et > self.getMinThreshold(self.jetThresholds), jets) # apply min energy threshold to remove low-energy candidates and reduce combinations to check
        if len(self.jetThresholds) == 1:
            jetCandidates = [[jet] for jet in jetCandidates]
        elif len(self.jetThresholds) == 2:
            jetCandidates = makeListOfPairings(jetCandidates)
        elif len(self.jetThresholds) == 3:
            jetCandidates = makeListOfTriplets(jetCandidates)
        elif len(self.jetThresholds) == 4:
            jetCandidates = makeListOfQuadruplets(jetCandidates)
        elif len(self.jetThresholds) == 5:
            jetCandidates = makeListOfQuintuplets(jetCandidates)
        elif len(self.jetThresholds) == 6:
            jetCandidates = makeListOfSextuplets(jetCandidates)
        else:
            print "Can only process up to 6 jets so far."
            raise NotImplementedError
        #for jetConstellation in jetCandidates:
        for eleIndex, elePart in enumerate(eleCandidates):
            if not self.applyEleThresholds(elePart): continue
            for tauIndex, tauPart in enumerate(tauCandidates): # iterate over combinations of the same number of taus as taus are required by the trigger
                if self.olr:
                    olr = True
                    for tau in tauPart:
                        olr = olr and tau.OLR(elePart, run)
                    if not olr: continue
                if not self.applyTauThresholds(tauPart): continue # if the current combination doesn't fulfil the trigger, try the next one
                for jetIndex, jetPart in enumerate(jetCandidates): # also iterate over jet combinations
                   # overlap removal with currently selected taus, if required by trigger (i.e. hyphen in L1 part of trigger name between tau and jet parts)
                   if self.olr:
                       olr = True
                       for jet in jetPart:
                           olr = olr and jet.OLR(tauPart, run)
                       if not olr: continue
                   if not self.applyJetThresholds(jetPart): continue
                   if self.applyTopoCuts(tauPart, jetPart): # if taus and jets fulfil energy thresholds, look at topological cuts
                       # also fulfilled? Then we're good.
                       #print "Found good topology, done"
                       #if run == "Run3": print "Winning combinations are", tauIndex, "for taus and", jetIndex, "for jets"
                       return True, elePart, tauPart, jetPart

        # Can only get here if we went through all combinations and couldn't make it work
        return False, [], [], []

    def applyEleThresholds(self, eles):
        return passCombinedTrigger(eles, self.eleThresholds)[0]

    def applyTauThresholds(self, taus):
        return passCombinedTrigger(taus, self.tauThresholds)[0]

    def applyJetThresholds(self, jets):
        return passCombinedTrigger(jets, self.jetThresholds)[0]

    def getMinThreshold(self, triggerList):
        if "notrig" in triggerList:
            return 0.
        else:
            if triggerList[0].startswith("L1_J"): # jet trigger case
                return 1000.*min([float(triggerName[4:]) for triggerName in triggerList])
            elif triggerList[0].startswith("L1_TAU"): # tau trigger case
                return 1000.*min([float(triggerName[6:].replace("IM", "")) for triggerName in triggerList])
            elif triggerList[0].startswith("L1_EM"): # electron trigger case
                return 1000.*min([float(triggerName[5:].replace("VHI", "")) for triggerName in triggerList])
            else:
                print "Don't understand given lead trigger name", triggerList[0]

    def applyTopoCuts(self, taus = None, jets = None):
        # Example for how to write these: J_DR_24 -> jets dr < 2.4 OR J_MJJ_500 -> jet max mjj > 500 GeV for all combinations of two jets
        passed = True
        for topoCut in self.topoCuts:
            cutOn = topoCut.split("_")[0]
            cutVar = topoCut.split("_")[1]
            cutVal = int(topoCut.split("_")[2])


            objToCut = None
            if cutOn == "J": # cutting on jets
                objToCut = list(jets)
            elif cutOn == "T": # cutting on taus
                objToCut = list(taus)
            elif cutOn in ["JT", "TJ"]:
                objToCut = list(jets + taus)
            else:
                print "Implemented options for topological cuts are either on the jets, the taus or all together."
                raise NotImplementedError

            if(cutVar == "DR"): # delta r
                assert len(objToCut) == 2, "Don't know how to apply DR cut, more than two dudes to check"
                passed = passed and (objToCut[0].p4.DeltaR(objToCut[1].p4) < cutVal/10.)
            elif(cutVar == "DE"): # delta eta
                assert len(objToCut) == 2, "Don't know how to apply DEta cut, more than two dudes to check"
                passed = passed and (abs(objToCut[0].eta - objToCut[1].eta) < cutVal/10.)
            elif(cutVar == "DP"): # delta phi
                assert len(objToCut) == 2, "Don't know how to apply DPhi cut, more than two dudes to check"
                passed = passed and (objToCut[0].p4.DeltaPhi(objToCut[1].p4) < cutVal/10.)
            elif(cutVar == "MJJ"): # max jetjet inv mass for all available combinations exceeds thresholds
                pairs = makeListOfPairings(objToCut)
                goodMjj = False
                for pair in pairs:
                    goodMjj = goodMjj or (pair[0].p4 + pair[1].p4).M() > cutVal*1000.
                passed = passed and goodMjj
            elif(cutVar == "ETA"): # abs eta value of all objects below threshold
                for obj in objToCut:
                    passed = passed and obj.eta < cutVal/10.
            else:
                print "Topo cut on variable", cutVar, "not implemented yet"
                raise NotImplementedError

        return passed

    def applyOfflCut(self, taus = None, jets = None):
        # apply trigger-induced cuts on offline quantities, like plateau cuts
        passed = True
        for offlCut in self.offlCuts:
            cutOn = offlCut.split("_")[0]
            cutVar = offlCut.split("_")[1]
            cutVal = offlCut.split("_")[2]
            cutVal = cutVal.split(":") if ":" in cutVal else [cutVal] # allow for multiple cut values on leading/subleading object

            objToCut = None
            if cutOn == "J": # cutting on jets
                objToCut = list(jets)
            elif cutOn == "T": # cutting on taus
                objToCut = list(taus)
            else:
                print "Implemented options for offline cuts are either on the jets or the taus. Mixing not included yet."
                raise NotImplementedError

            if(cutVar == "PT"):
                for index, cutThresh in enumerate(cutVal):
                    #print "Threshold", index, "is", float(cutThresh)*1000., "the jet pt is", objToCut[index].pt
                    passed = passed and (objToCut[index].pt > float(cutThresh)*1000.)
                    #print "Result:", objToCut[index].pt > float(cutThresh)*1000.
            elif(cutVar == "DR"):
                assert len(cutVal) == 1, "Two different delta R cuts on the same type of objects? Sounds weird"
                #print "DR val is", objToCut[0].p4.DeltaR(objToCut[1].p4), "thresh is", float(cutVal[0])/10.
                passed = passed and (objToCut[0].p4.DeltaR(objToCut[1].p4) < float(cutVal[0])/10. )
            else:
                print "Offline cut on variable " + cutVar + " not implemented yet"
                raise NotImplementedError

        return passed

######## functions meant for use in main script

def initHist(name, xlabel, nbins, lowEdge, highEdge):
    hist = R.TH1F(name, name, nbins, lowEdge, highEdge)
    hist.GetXaxis().SetTitle(xlabel)
    hist.GetYaxis().SetTitle("Number of Events")
    hist.Sumw2()
    return hist

def tauId(wp, nProngs, score):
    assert wp in ["loose", "medium", "tight"], "Working point " + wp + " not implemented"
    assert nProngs in [1, 3], "Offline ID only available for 1p and 3p taus, not " + str(nProngs) + "p"
    threshDict = {} # numbers from table 2 in http://cds.cern.ch/record/2688062/files/ATL-PHYS-PUB-2019-033.pdf
    threshDict[1] = {"loose" : 0.15, "medium" : .25, "tight" : .40}
    threshDict[3] = {"loose" : 0.25, "medium" : .40, "tight" : .55}

    return (score > threshDict[nProngs][wp])

def imIso(et, iso):
    if et > 60000.:
        return True
    else:
        return bool(iso < et/10 + 2000.)


def buildL1Taus(etBranch, etaBranch, phiBranch, isoBranch):
    # first convert branch output into float
    etList = [val for val in etBranch]
    etaList = [val for val in etaBranch]
    phiList = [val for val in phiBranch]
    isoList = [val for val in isoBranch]

    # check that they all have the same length, otherwise weird things might happen
    assert (len(etList) == len(etaList) and len(etaList) == len(phiList) and len(phiList) == len(isoList)), "Et, eta, phi and iso don't have same number of entries, fishy!"

    # abort if no taus in event
    if len(etList) == 0: return []

    # order by et and build tau objects
    tauList = []
    zipped = list(zip(etList, etaList, phiList, isoList))
    zipped.sort()
    zipped = list(reversed(zipped)) # go from highest et to lowest
    for et, eta, phi, iso in zipped:
        tauList.append(L1Tau(et, eta, phi, iso))
    return tauList

def buildL1Eles(etBranch, etaBranch, phiBranch, isoBranch, run):
    # first convert branch output into float
    etList = [val if run == "Run2" else 1000.*val for val in etBranch]
    etaList = [val for val in etaBranch]
    phiList = [val for val in phiBranch]
    isoList = [val for val in isoBranch]

    # check that they all have the same length, otherwise weird things might happen
    assert (len(etList) == len(etaList) and len(etaList) == len(phiList) and len(phiList) == len(isoList)), "Et, eta, phi and iso don't have same number of entries, fishy!"

    # abort if no electrons in event
    if len(etList) == 0: return []

    # order by et and build tau objects
    eleList = []
    zipped = list(zip(etList, etaList, phiList, isoList))
    zipped.sort()
    zipped = list(reversed(zipped)) # go from highest et to lowest
    for et, eta, phi, iso in zipped:
        eleList.append(L1Ele(et, eta, phi, iso))
    return eleList

def buildOfflTaus(ptBranch, etaBranch, phiBranch, ntracksBranch, scoreBranch):
    ptList = [val for val in ptBranch]
    etaList = [val for val in etaBranch]
    phiList = [val for val in phiBranch]
    ntracksList = [val for val in ntracksBranch]
    scoreList = [val for val in scoreBranch]

    # abort if no taus in event
    if len(ptList) == 0: return []

    # order by et and build tau objects
    tauList = []
    zipped = list(zip(ptList, etaList, phiList, ntracksList, scoreList))
    zipped.sort()
    zipped = list(reversed(zipped)) # go from highest et to lowest
    for pt, eta, phi, ntracks, score in zipped:
        tauList.append(OfflTau(pt, eta, phi, ntracks, score))
    return tauList

def buildOfflEles(ptBranch, etaBranch, phiBranch, ptconeBranch, etconeBranch, flagBranch, tightflagBranch, isoflagBranch):
    ptList = [val for val in ptBranch]
    etaList = [val for val in etaBranch]
    phiList = [val for val in phiBranch]
    ptconeList = [val for val in ptconeBranch]
    etconeList = [val for val in etconeBranch]
    flagList = [val for val in flagBranch]
    tightflagList = [val for val in tightflagBranch]
    isoflagList = [val for val in isoflagBranch]

    # abort if no electrons in event
    if len(ptList) == 0: return []

    # order by et and build tau objects
    eleList = []
    zipped = list(zip(ptList, etaList, phiList, ptconeList, etconeList, flagList, tightflagList, isoflagList))
    zipped.sort()
    zipped = list(reversed(zipped)) # go from highest et to lowest
    for pt, eta, phi, ptcone, etcone, flag, tightflag, isoflag in zipped:
        eleList.append(OfflEle(pt, eta, phi, ptcone, etcone, flag, tightflag, isoflag))
    return eleList

def buildTruthTaus(ptBranch, etaBranch, phiBranch):
    ptList = [val for val in ptBranch]
    etaList = [val for val in etaBranch]
    phiList = [val for val in phiBranch]

    # abort if no taus in event
    if len(ptList) == 0: return []

    # order by et and build tau objects
    tauList = []
    zipped = list(zip(ptList, etaList, phiList))
    zipped.sort()
    zipped = list(reversed(zipped)) # go from highest et to lowest
    for pt, eta, phi in zipped:
        tauList.append(TruthTau(pt, eta, phi))
    return tauList

def buildTruthEles(ptBranch, etaBranch, phiBranch):
    ptList = [val for val in ptBranch]
    etaList = [val for val in etaBranch]
    phiList = [val for val in phiBranch]

    # abort if no electrons in event
    if len(ptList) == 0: return []

    # order by et and build tau objects
    eleList = []
    zipped = list(zip(ptList, etaList, phiList))
    zipped.sort()
    zipped = list(reversed(zipped)) # go from highest et to lowest
    for pt, eta, phi in zipped:
        eleList.append(TruthEle(pt, eta, phi))
    return eleList

def buildL1Jets(etBranch, etaBranch, phiBranch):
    etList = [val for val in etBranch]
    etaList = [val for val in etaBranch]
    phiList = [val for val in phiBranch]

    # abort if no jets in event
    if len(etList) == 0: return []

    # order by et and build jet objects
    jetList = []
    zipped = list(zip(etList, etaList, phiList))
    zipped.sort()
    zipped = list(reversed(zipped)) # go from highest et to lowest
    for et, eta, phi in zipped:
        jetList.append(L1Jet(et, eta, phi))
    return jetList

def buildOfflJets(ptBranch, etaBranch, phiBranch):
    ptList = [val for val in ptBranch]
    etaList = [val for val in etaBranch]
    phiList = [val for val in phiBranch]

    # abort if no jets in event
    if len(ptList) == 0: return []

    # order by et and build jet objects
    jetList = []
    zipped = list(zip(ptList, etaList, phiList))
    zipped.sort()
    zipped = list(reversed(zipped)) # go from highest et to lowest
    for pt, eta, phi in zipped:
        jetList.append(OfflJet(pt, eta, phi))
    return jetList


def passCombinedTrigger(objs, trigs):
    trigList = list(trigs) # shallow copy
    usedObjs = []
    for trigger in trigs:
        objIndex = -1
        for obj in objs:
            objIndex += 1
            if objIndex in usedObjs: continue
            if obj.fulfils(trigger):
                usedObjs.append(objIndex)
                trigList.remove(trigger)
                break
        if objIndex in usedObjs: continue
        else: break
    triggereds = [objs[i] for i in usedObjs]
    return bool(len(trigList) == 0), triggereds

def initObjHistDict(objTitle):
    dict = {}
    etName, etBins, etLow, etHigh = _binningDict["Et"]
    dict["et"] = initHist(objTitle + "_et", "E_{T} [MeV]", etBins, etLow, etHigh)
    for quan in ["eta", "phi", "iso"]:
        if quan == "iso" and "jet" in objTitle: continue
        quanName, quanBins, quanLow, quanHigh = _binningDict[quan]
        dict[quan] = initHist(objTitle + "_" + quan, quanName, quanBins, quanLow, quanHigh)

    return dict

# it's likely easy to generalise this and make one function that covers all of these...
def makeListOfPairings(objList, minEnergy = 0.):
    pairList = []
    if len(objList) < 2: return pairList
    for i, iobj in enumerate(list(objList)):
        for j, jobj in enumerate(list(objList)):
            if i == j or i > j or jobj.et < minEnergy: continue # this way, i < j always and first object in list has higher Et. Also, only look at objs that at least fulfil the lowest energy threshold
            pairList.append([iobj, jobj])
    return pairList

def makeListOfTriplets(objList, minEnergy = 0.):
    tripList = []
    if len(objList) < 3: return tripList
    for i, iobj in enumerate(list(objList)):
        for j, jobj in enumerate(list(objList)):
            if i == j or i > j: continue
            for k, kobj in enumerate(list(objList)):
                if j == k or j > k or kobj.et < minEnergy: continue
                tripList.append([iobj, jobj, kobj])
    return tripList

def makeListOfQuadruplets(objList, minEnergy = 0.):
    quadList = []
    if len(objList) < 4: return quadList
    for i, iobj in enumerate(list(objList)):
        for j, jobj in enumerate(list(objList)):
            if i == j or i > j: continue
            for k, kobj in enumerate(list(objList)):
                if j == k or j > k: continue
                for l, lobj in enumerate(list(objList)):
                    if k == l or k > l or lobj.et < minEnergy: continue
                    quadList.append([iobj, jobj, kobj, lobj])
    return quadList

def makeListOfQuintuplets(objList, minEnergy = 0.):
    quinList = []
    if len(objList) < 5: return quinList
    for i, iobj in enumerate(list(objList)):
        for j, jobj in enumerate(list(objList)):
            if i == j or i > j: continue
            for k, kobj in enumerate(list(objList)):
                if j == k or j > k: continue
                for l, lobj in enumerate(list(objList)):
                    if k == l or k > l: continue
                    for m, mobj in enumerate(list(objList)):
                        if l == m or l > m or mobj.et < minEnergy: continue
                        quinList.append([iobj, jobj, kobj, lobj, mobj])
    return quinList

def makeListOfSextuplets(objList, minEnergy = 0.):
    sextList = []
    if len(objList) < 6: return sextList
    for i, iobj in enumerate(list(objList)):
        for j, jobj in enumerate(list(objList)):
            if i == j or i > j: continue
            for k, kobj in enumerate(list(objList)):
                if j == k or j > k: continue
                for l, lobj in enumerate(list(objList)):
                    if k == l or k > l: continue
                    for m, mobj in enumerate(list(objList)):
                        if l == m or l > m: continue
                        for n, nobj in enumerate(list(objList)):
                            if m == n or m > n or nobj.et < minEnergy: continue
                        sextList.append([iobj, jobj, kobj, lobj, mobj, nobj])
    return sextList

def calculateEffs(integralDict):
    for run, trigDict in integralDict.iteritems():
        print "*"*50
        print run.replace("n", "n "), "efficiencies"
        print "*"*50
        denom = trigDict["denom"]
        if abs(denom) < 1e-6:
            print "No events passing denom selection, cannot construct efficiencies"
        else:
            for trigName, num in trigDict.iteritems():
                if trigName == "denom": continue
                print trigName, num/denom
        print ""

def getXsec(sampleName):
    dsid = _dsidDict[sampleName]
    with open("XSections_13TeV.txt", "r") as xFile:
        for xLine in xFile:
            if xLine.startswith(dsid):
                splitLine = xLine.split()
                xsecBRfeff = float(splitLine[1])*float(splitLine[2])*float(splitLine[3])
                return xsecBRfeff

    print "Cross-section for sample", sampleName, "not found"
    return -1.

def readSumOfWeights(sampleName, weightFileName):
    call(["touch", weightFileName])
    with open(weightFileName, "r") as weightFile:
        for wLine in weightFile:
            if wLine.startswith(sampleName):
                return float(wLine.split()[-1])
    print "Sum of weights for sample", sampleName, "not found"
    return 0.

######## functions/objects to be called only within above functions
######## (marked with a "_" at the start)

_binningDict = {
    "Et" : ("E_{T}", 30, 0., 150000.),
    "eta" : ("#eta", 26, -4.4, 4.4),
    "phi" : ("#phi", 32, -3.2, 3.2),
    "iso" : ("EM Isolation", 25, 0., 25000.),
    "mu" : ("<#mu>", 20, 0, 80)
}

_dsidDict = {"ggF1lh" : "600029", "vbf1" : "502982", "jz0w" : "361020"}
